<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	const POSITIONS = [
		'CEO' => ['President'],
		'VP' => ['VP Marketing', 'VP Sales'],
		'Sale manager' => ['Sales Manager (APAC)', 'Sale Manager (EMEA)', 'Sales Manager (NA)']
	];

	const NUMBER_KEYS = [
		'sales', 'numberOfCustomerBought', 'commission', 'totalCommission', 'totalSales', 'quantity'
	];

	private $maxUniqueJobTitleCnt;

	public function __construct()
    {
        parent::__construct();
		$this->load->model('base_model');
    }

	public function index()
	{	
		$arr = [
			'Functions Available' => [
				1 => 'organizations',
				2 => 'offices',
				3 => 'sales_report/{employeeNumber}'
			]
		];
		
		$this->displayResult($arr);
	}

	public function organizations() {
		$teamLeads = $this->base_model->getTeamLeads();
		$jobTitles = $this->base_model->getJobTitles();
		$temp = [];

		foreach ($teamLeads as $key => &$teamLead) {
			$temp[$teamLead['employeeNumber']] = $teamLead;
			$temp[$teamLead['employeeNumber']]['employeeUnder'] = $this->base_model->getEmployees($teamLead['employeeNumber'], $teamLead['employeeNumber']);

			if (count($temp[$teamLead['employeeNumber']]['employeeUnder']) == 0) unset($temp[$teamLead['employeeNumber']]);
		}

		#release memory load
		unset($teamLeads, $jobTitles);

		$this->maxUniqueJobTitleCnt = count($temp);
		$this->proccessTeamList($temp);
		$this->displayResult(reset($temp));
	}

	/**
	 * 	array 	$temp	list of unique positions
	 * 	int 	$cnt 	used to check if all positions are already pushed to designated array
	 */
	private function proccessTeamList(&$temp, $cnt = 0) {
		foreach (array_reverse($temp) as $key => $teamLead) {
			if ( ! isset($teamLead['reportsTo'])) continue;

			foreach (self::POSITIONS as $possitionArr) {
				#crawl until sale manager positions: if found push the employee under list
				if (in_array($teamLead['jobTitle'], $possitionArr) && $this->maxUniqueJobTitleCnt > count($temp)) {
					$this->reprocessNestedTeamList($temp, $teamLead, true);
				}else{
					#rearrane VP positions
					$this->reprocessNestedTeamList($temp, $teamLead);
				}
			}
		}

		if (count($temp) > 1) { #recall this function until the arr count is 1 which is the president
			$cnt++;
			if ($cnt == $this->maxUniqueJobTitleCnt) return false; #avoid infinite loop
			$this->proccessTeamList($temp, $cnt);
		}
	}

	/**
	 * 	array 	$temp	list of unique positions
	 *  array 	$teamLead contains specific teamLead data
	 *  boolean $crawl	used to check if there's a need of crawling arrays to push the data
	 */
	private function reprocessNestedTeamList(&$temp, $teamLead, $crawl = null) {
		if ( ! isset($temp[$teamLead['reportsTo']]['employeeUnder']) && ! isset($crawl)) return false;

		if ($crawl) { #can crawl until 3rd nested array
			foreach ($temp as $key => &$teamleadArr) {
				if (isset($teamleadArr['employeeUnder']) && count($teamleadArr['employeeUnder']) > 0) {

					foreach ($teamleadArr['employeeUnder'] as $key => &$employeeUnderArr) {
						if ($teamLead['reportsTo'] === $employeeUnderArr['employeeNumber']) {
							#get array key and push the data to the key
							$empKeyNumber = array_search($teamLead['employeeNumber'], array_column($employeeUnderArr['employeeUnder'], 'employeeNumber'));

							foreach ($teamLead['employeeUnder'] as $key => $value) {
								unset($value['reportsTo']);
								$employeeUnderArr['employeeUnder'][$empKeyNumber]['employeeUnder'][$key] = $value;
							}

							unset($temp[$teamLead['employeeNumber']], $employeeUnderArr['employeeUnder'][$empKeyNumber]['reportsTo']);
						}
					}
					unset($teamleadArr['reportsTo']);
				}

			}
		}else{
			foreach ($temp[$teamLead['reportsTo']]['employeeUnder'] as &$val) {
				unset($val['reportsTo']);

				if ($val['employeeNumber'] != $teamLead['employeeNumber']) return false;

				$val['employeeUnder'] = $teamLead['employeeUnder'];
				unset($temp[$teamLead['employeeNumber']]);
			}
		}

	}

	public function offices() {
		$offices = $this->base_model->getOffices();
		$employees = $this->base_model->getEmployees(null, null, true);

		foreach ($offices as &$office) {
			foreach ($employees as $key => $employee) {
				if ($office['officeCode'] == $employee['officeCode']) {
					unset($employee['officeCode'],$employee['reportsTo']);
					$office['employees'][$key] = $employee;
				}
			}
		}

		#release memory load
		unset($employees);
		$this->displayResult($offices);
	}

	public function sales_report($employeeNumber = null) {
		if (empty($employeeNumber)) die("Employee Number required!");

		$result = $this->base_model->getSalesReport($employeeNumber);
		$result['productLines'] = $this->base_model->getProductLinesByEmployeeNumber($employeeNumber);

		foreach ($result['productLines'] as $key => &$productLineArr) {
			$productDetails = $this->base_model->getProductByEmpNumByProdLine($employeeNumber, $productLineArr['productLine']);
			$tempCommission = 0;

			foreach ($productDetails as $key => &$productDetailArr) {
				$percentage = explode(':', $productDetailArr['productScale'])[1];
				$productDetailArr['commission'] = round(($productDetailArr['quantityOrdered'] / $percentage) * $productDetailArr['priceEach']);

				#remove not required fields
				unset($productDetailArr['priceEach'], $productDetailArr['quantityOrdered'], $productDetailArr['productScale']);
				$tempCommission += $productDetailArr['commission'];

				$this->convertStringToNumber($productDetailArr);
			}

			$productLineArr['products'] = $productDetails;
			$productLineArr['commission'] = $tempCommission;
			array_multisort($productLineArr );
			$this->convertStringToNumber($productLineArr);
		}

		$result['totalCommission'] = array_sum(array_column($result['productLines'], 'commission'));
		$this->convertStringToNumber($result);
		array_multisort($result);

		$this->displayResult($result);
	}

	/**
	 * array 	$arr 	convert disignated key value to number
	 */
	private function convertStringToNumber(&$arr){
		foreach ($arr as $key => $value) 
			if (in_array($key, self::NUMBER_KEYS)) $arr[$key] = (float) $arr[$key];
	}

	private function displayResult($result) {
		print_r(json_encode($result));
	}
}
