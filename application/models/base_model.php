<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Base_model extends CI_Model {

	const ORDER_STATUS_SHIPPED = 'Shipped';

    public function getOffices() {
	    $this->db->select('officeCode, city');
	    $result = $this->db->get('offices');

	    return $result->result_array();
	}

	public function getEmployees($reportsTo = null, $employeeNumber = null, $officeCode = null) {
		$select = 'employeeNumber, CONCAT(firstname, " ", lastname) as name, jobTitle, reportsTo';
		$select = isset($officeCode) ? $select . ',officeCode' : $select;

	    $this->db->select($select);

	    if ($reportsTo)
	    	$this->db->where('reportsTo', $reportsTo)->where('employeeNumber !=', $employeeNumber);

	    $result = $this->db->get('employees');
	    return $result->result_array();
	}
	
	public function getTeamLeads() {
	    $this->db->select('employeeNumber, CONCAT(firstname, " ", lastname) as name, jobTitle, reportsTo');
	    $this->db->group_by('jobTitle');
	    $result = $this->db->get('employees');

	    return $result->result_array();
	}

	public function getJobTitles() {
		$result = $this->db->select('jobTitle, employeeNumber')->group_by('jobTitle')->get('employees');
		return $result->result_array();
	}

	public function getSalesReport($employeeNumber = null) {
		$this->db->select('
			emp.employeeNumber,
			CONCAT(firstname, " ", lastname) as name,
		 	jobTitle,
		 	emp.officeCode,
		 	offices.city,
		 	sum(quantityOrdered * priceEach) as totalSales
		 ');
		$this->db->from('employees as emp');
		$this->db->join('offices', 'offices.officeCode = emp.officeCode');
		$this->db->join('customers', 'customers.salesRepEmployeeNumber = emp.employeeNumber', 'left');
		$this->db->join('orders', 'orders.customerNumber = customers.customerNumber', 'left');
		$this->db->join('orderdetails', 'orderdetails.orderNumber = orders.orderNumber', 'left');

		if ($employeeNumber)
			$this->db->where('employeeNumber', $employeeNumber);
		
		$this->db->where('orders.status', self::ORDER_STATUS_SHIPPED);

		$result = $this->db->get();
		return $result->row_array();
	}

	public function getProductLinesByEmployeeNumber($employeeNumber) {
		$this->db->select('
			productlines.productLine,
			productlines.textDescription,
			sum(products.quantityInStock) as quantity,
			sum(quantityOrdered * priceEach) as sales
		');
		$this->db->from('employees as emp');
		$this->db->join('customers', 'customers.salesRepEmployeeNumber = emp.employeeNumber', 'left');
		$this->db->join('orders', 'orders.customerNumber = customers.customerNumber', 'left');
		$this->db->join('orderdetails', 'orderdetails.orderNumber = orders.orderNumber', 'left');
		$this->db->join('products', 'products.productCode = orderdetails.productCode', 'left');
		$this->db->join('productlines', 'productlines.productLine = products.productLine', 'left');
		$this->db->group_by('productlines.productLine');
		$this->db->where('employeeNumber', $employeeNumber);
		
		$result = $this->db->get();
		return $result->result_array();
	}

	public function getProductByEmpNumByProdLine($employeeNumber, $productLine) {
		$this->db->select('
			products.productCode,
			products.productName,
			quantityInStock as quantity,
			sum(quantityOrdered * priceEach) as sales,
			count(orders.customerNumber) as numberOfCustomerBought,
			productScale,
			quantityOrdered,
			priceEach
		');
		$this->db->from('employees as emp');
		$this->db->join('customers', 'customers.salesRepEmployeeNumber = emp.employeeNumber', 'left');
		$this->db->join('orders', 'orders.customerNumber = customers.customerNumber', 'left');
		$this->db->join('orderdetails', 'orderdetails.orderNumber = orders.orderNumber', 'left');
		$this->db->join('products', 'products.productCode = orderdetails.productCode', 'left');
		$this->db->group_by('products.productCode');
		$this->db->where('employeeNumber', $employeeNumber);
		$this->db->where('products.productLine', $productLine);

		$result = $this->db->get();
		return $result->result_array();
	}
 }
